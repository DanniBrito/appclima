//
//  ViewController.swift
//  AppClima
//
//  Created by Danni Brito on 25/10/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var usuarioTextFieldLogin: UITextField!
    
    @IBOutlet weak var contraseñaTextFieldLogin: UITextField!
    
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewWillAppear(_ animated: Bool) {
        usuarioTextFieldLogin.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        usuarioTextFieldLogin.text = ""
        contraseñaTextFieldLogin.text = ""
    }
    

    //MARK:- Actions
    
    @IBAction func entrarBotonLogin(_ sender: Any) {
        let user = usuarioTextFieldLogin.text ?? ""
        let password = contraseñaTextFieldLogin.text ?? ""
        
        switch (user,password) {
        case ("d","d"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
        case ("d",_):
            mostrarAlerta(mensaje: "contraseña incorrecta")
        default:
            mostrarAlerta(mensaje: "usuario y/o contraseña incorrectos")
        }
    }
    
    @IBAction func entrarInvitadoBotonLogin(_ sender: Any) {
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "Error", message: mensaje, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "aceptar", style: .default) { (action) in
            self.usuarioTextFieldLogin.text = ""
            self.contraseñaTextFieldLogin.text = ""
        }
        
        alertView.addAction(aceptar)
        
        present(alertView,animated: true,completion: nil)
    }
    
    
}

