//
//  invitadoViewController.swift
//  AppClima
//
//  Created by Danni Brito on 25/10/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit
import CoreLocation

class invitadoViewController: UIViewController, CLLocationManagerDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var resulUbicacionLabel: UILabel!
    @IBOutlet weak var nameCityInvitado: UILabel!
    
    let locationManager = CLLocationManager()
    
    var bandera = false
    
    //MARK:- ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
        }
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
        consultarPorUbicacion(lat: (location?.latitude)!,lon: (location?.longitude)!)
        locationManager.stopUpdatingLocation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Actions
    
    private func consultarPorUbicacion(lat:Double,lon:Double){
        let service = Servicio()
        service.consultaPorUbicacion(lat: lat, lon: lon) { (weather,city) in
            DispatchQueue.main.async {
                self.resulUbicacionLabel.text = weather
                self.nameCityInvitado.text = city
            }
        }
        
        
    }
    
}
