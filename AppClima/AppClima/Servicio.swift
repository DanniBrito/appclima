//
//  Servicio.swift
//  AppClima
//
//  Created by Danni Brito on 7/11/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import Foundation

class Servicio{
    func consultaPorCiudad(city: String, completion:@escaping (String)->()){
        let urlSTR = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=498e30a73cd9cd3f1b86bb3e7bb2b4f5"
        consulta(urlStr: urlSTR) { (weather) in
            completion(weather)
        }
    }
    
    func consultaPorUbicacion(lat:Double, lon:Double, completion:@escaping (String,String)->()){
        let urlSTR = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=498e30a73cd9cd3f1b86bb3e7bb2b4f5"
        consulta(urlStr: urlSTR) { (weather,city) in
            completion(weather,city)
        }
    }
    
    func consulta(urlStr:String, completion:@escaping (String,String)-> ()){
        let urlSTR = urlStr
        
        let url = URL(string: urlSTR)
        
        let request = URLRequest(url: url!)
        
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            print("Error: \(String(describing: error))")
            if let _ = error {
                return
            }
            
            do{
                let weatherJson = try JSONSerialization.jsonObject(with: data!, options: [])
                let weatherDict = weatherJson as! NSDictionary
                guard let weatherKey = weatherDict["weather"] as? NSArray else {
                    completion("Ciudad no válida")
                    return
                }
                let weather = weatherKey[0] as! NSDictionary
                completion("\(weather["description"] ?? "Error")","\(weatherDict["name"] ?? "Error")")
                
            } catch{
                print("Error al generar el Json")
            }
            
            
        }
        task.resume()    }
}
