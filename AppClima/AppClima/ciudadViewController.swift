//
//  ciudadViewController.swift
//  AppClima
//
//  Created by Danni Brito on 31/10/17.
//  Copyright © 2017 Danni Brito. All rights reserved.
//

import UIKit

class ciudadViewController: UIViewController {

    
    //MARK: - Outlets
    
    @IBOutlet weak var ciudadTextField: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    //MARK: - ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - Actions
    
    override func viewWillAppear(_ animated: Bool) {
        ciudadTextField.becomeFirstResponder()
    }

    
    //MARK: - Actions
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
    
        let service = Servicio()
        service.consultaPorCiudad(city: ciudadTextField.text!) { (weather) in
            DispatchQueue.main.async {
                self.resultLabel.text = weather
            }
        }
        
    }
    
    
    
    

    

}
